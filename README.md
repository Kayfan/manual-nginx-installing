# Инструкция по установке и настройке NGINX

Это руководство поможет вам установить и настроить NGINX с использованием пользовательского конфигурационного файла, виртуального хоста и сайта на сервере Ubuntu.

## Требования

- Установленная система Ubuntu
- Права пользователя с возможностью использования команды sudo

## Установка NGINX

Для установки NGINX выполните следующие команды:
```
sudo apt update
sudo apt install nginx
```

## Настройка пользователя для NGINX

Создайте пользователя nginxuser и директорию для файлов сайта:
```
sudo adduser --system --no-create-home --disabled-login --disabled-password --group nginxuser
sudo mkdir -p /home/nginxuser/wwwfiles
sudo chown nginxuser:nginxuser /home/nginxuser/wwwfiles
```

## Клонирование репозитория Git

Клонируйте ваш репозиторий в удобное место на сервере. Например, в /tmp/my_nginx_config:
```
cd /tmp
git clone URL_РЕПОЗИТОРИЯ.git
```

## Настройка NGINX

Скопируйте ваш конфигурационный файл и директорию виртуального хоста в директорию NGINX:
```
sudo cp /tmp/mynginxconfig/nginx.conf /etc/nginx/nginx.conf
sudo cp -r /tmp/mynginxconfig/sites-available /etc/nginx/sites-available
```

Настраиваем символьные ссылки для виртуальных хостов:
```
sudo ln -s /etc/nginx/sites-available/chigur /etc/nginx/sites-enabled/chigur
```

Скопируйте файлы сайта в директорию /home/nginxuser/wwwfiles:
```
sudo cp -r /tmp/mynginxconfig/site/* /home/nginxuser/wwwfiles/
sudo chown -R nginxuser:nginxuser /home/nginxuser/wwwfiles
```

Убедитесь, что нет ошибок конфигурации:
```
sudo nginx -t
```

Если сообщение об ошибке не появилось, можно перезапустить NGINX для применения изменений:
```
sudo systemctl reload nginx
```

Теперь NGINX должен быть успешно установлен и настроен.

## Ссылки на документацию

[Nginx documentatio](https://nginx.org/en/docs/)
[О Nginx от Skillbox](https://skillbox.ru/media/code/server-nginx-kak-on-rabotaet-i-kak-ego-nastroit/)